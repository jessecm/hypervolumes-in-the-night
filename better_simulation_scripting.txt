### run as a loop, assuming all julia and R scripts are in the same directory
### below, the code is for 10 runs... obviously, you might want to do more, but good to start small
### to create an alias for julia see: https://julialang.org/downloads/platform/
### make sure your terminal will call the julia language with "julia" (line ~50 : julia juliaBetterDistances.jl )

for run in {1..10}; do (

Rscript HITN_extendedPhylogeneticMatrix_philopatric_cauchy_sameParameters.R

sed -e 's/".*",//' allEvoOutput.csv > allEvoOutput1.csv 
tail -n +2 "allEvoOutput1.csv" > "$FILE.tmp" && mv "$FILE.tmp" "allEvoOutput2.csv"

sed -e 's/".*",//' traitSpaceOutput.csv > traitSpaceOutput1.csv 
tail -n +2 "traitSpaceOutput1.csv" > "$FILE.tmp" && mv "$FILE.tmp" "traitSpaceOutput2.csv"

julia HITN_juliaBetterDistances.jl


); done


