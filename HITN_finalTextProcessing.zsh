#!/bin/zsh

sed -e 's/".*"//' all_dists.csv > all_dists1.csv
tr -d ',' < all_dists1.csv > all_dists2.csv
tail -n +2 "all_dists2.csv" > "$FILE.tmp" && mv "$FILE.tmp" "all_dists3.csv"
sed 's/[^1]*\(1.*\)/\1/' all_dists3.csv > all_dists4.csv
awk '{ print length($0) }' all_dists4.csv > all_dists5.csv
