everydist = read.table(file = "HITNjuliaDistOutput.csv", header = T, sep = ",")

plot(everydist$D512 ~ sqrt(everydist$phyl), col = "blue")
abline(lm(everydist$D512 ~ sqrt(everydist$phyl)), col = "blue")
points(everydist$D128 ~ sqrt(everydist$phyl), col = "darkgreen")
abline(lm(everydist$D128 ~ sqrt(everydist$phyl)), col = "darkgreen")
points(everydist$D256 ~ sqrt(everydist$phyl), col = "violet")
abline(lm(everydist$D256 ~ sqrt(everydist$phyl)), col = "violet")
points(everydist$D64 ~ sqrt(everydist$phyl), col = "goldenrod")
abline(lm(everydist$D64 ~ sqrt(everydist$phyl)), col = "goldenrod")

library(ggplot2)
tallDists = read.table(file = "tallDistsSubset.csv", sep = ",", header = T)
dimDistNames = read.table(file = "dimensionDistanceNames.txt", header = T, sep = "\t")

tallDistsPlot <- ggplot(tallDists, aes(sqrt(phyl), value, colour = variable)) + 
	theme_minimal() +
	geom_jitter() +
	geom_point(size = 0.2) + 
	scale_colour_brewer(palette = "Dark2")
	labs(title = "tallDistsPlot") 
	
plot(tallDistsPlot)

### create a lookup table to make a column of numeric variables for dimensionality in tallDists
dimsNumeric = data.frame(variable = c("D1", "D2", "D4", "D8", "D16", "D32", "D64", "D128", "D256", "D512", "D1024"), dimNum = c(1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024))

library(tibble)
library(dplyr)

tallDistsNumDims = tallDists %>%
	inner_join(dimsNumeric)
	
tallDistsSmallDims = subset(tallDistsNumDims, dimNum <65)
tallDistsBigDims = data.frame(rbind(subset(tallDistsNumDims, dimNum >63), subset(tallDistsNumDims, dimNum <2)))

smallDimsPlot <- ggplot(tallDistsSmallDims, aes(sqrt(phyl), value, colour = as.character(dimNum))) + 
	theme_minimal() +
	geom_jitter() +
	geom_point(size = 0.2) + 
	scale_colour_brewer(palette = "Dark2")
	labs(title = "SmallDimsPlot") 
	
plot(smallDimsPlot)

tahneePalette=c("D1"="#F5F7B2", "D64"="#D2E59C", "D128"="#8FD9A8", "D256"="#28B5B5", "D512"="#4B778D", "D1024"="#2B4F60" )
### thanks to Tahnee Ames for the color suggestions! https://github.com/tahneeames 

### note below the reorder function just allows for numeric ordering in the legend instead of D64 coming after D512, etc. 
dev.new()
bigDimsPlot <- ggplot(tallDistsBigDims, aes(sqrt(phyl), value, colour = reorder(variable, dimNum))) + 
	theme_minimal() +
	geom_jitter() +
	geom_point(size = 0.2) + 
	scale_color_manual(values= tahneePalette)
	labs(title = "tallDistsPlot") 
	
plot(bigDimsPlot)


library(groupdata2)
longDists = read.table(file = "HITNjuliaLongDistOutput.csv", header = T, sep = ",")
dimDistNames = read.table(file = "dimensionDistanceNames.txt", header = T, sep = "\t")

tallDistsGoodNames = longDists %>%
	inner_join(dimDistNames)


tallDistsGoodNamesEuc = subset(tallDistsGoodNames, tallDistsGoodNames $distanceType == "euclidean")
tallDistsGoodNamesCheb = subset(tallDistsGoodNames, tallDistsGoodNames $distanceType == "chebyshev")

tallDistsGoodNamesEucDownsampled = downsample(tallDistsGoodNamesEuc, cat_col="phyl")
tallDistsGoodNamesChebDownsampled = downsample(tallDistsGoodNamesCheb, cat_col="phyl")

(chebDistsPlot <- ggplot(data = tallDistsGoodNamesChebDownsampled, aes(x = sqrt(phyl), y = value)) +
	geom_jitter(aes(colour = (log2(dimensionNumber))), size = 0.5)+ labs(title = "Chebyshev distance vs. square root of phylogenetic distance"))  
	    

chebDistsPlot + scale_colour_viridis_c() + theme_minimal()

dev.new()
(eucDistsPlot <- ggplot(data = tallDistsGoodNamesEucDownsampled, aes(x = sqrt(phyl), y = value)) +
	geom_jitter(aes(colour = (log2(dimensionNumber))), size = 0.5)+ labs(title = "Euclidean distance vs. square root of phylogenetic distance"))  
	    

eucDistsPlot + scale_colour_viridis_c() + theme_minimal()


(chebDistsPlot <- ggplot(data = tallDistsGoodNamesChebDownsampled, aes(x = log2(dimensionNumber), y = value)) +
	geom_jitter(aes(colour = (sqrt(phyl))), size = 0.5)+ labs(title = "Chebyshev distance vs. log2 trait dimensionality"))  
	    

chebDistsPlot + scale_colour_viridis_c() + theme_minimal()


tallDistsGoodNamesChebDownsampled16 = subset(tallDistsGoodNamesChebDownsampled, tallDistsGoodNamesChebDownsampled$phyl == 16)

tallDistsGoodNamesChebDownsampled4 = subset(tallDistsGoodNamesChebDownsampled, tallDistsGoodNamesChebDownsampled$phyl == 4)

tallDistsGoodNamesChebDownsampled2 = subset(tallDistsGoodNamesChebDownsampled, tallDistsGoodNamesChebDownsampled$phyl == 2)

