### similar idea to stepDividerTables in R, but this one is implemented in Julia
### still playing around with the right ratio of for loop runs to matrix size... 
### larger matrices take up more memory, but can make find new direction combos in fewer loops

### I think this is still a useful idea, because it lets me play with step size distribution more...
### but ultimately I should probably just do a version where evolutionary steps are drawn from a 
### multivariate normal distribution... either directly, or use them as multipliers for the 
### evolution step size magnitude... 


### case for 2D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions


goodOnes2_output = DataFrame(I(2), :auto);
goodOnes2_output = convert.(Float64, goodOnes2_output);
goodOnes2_output = @bind_rows(goodOnes2_output, goodOnes2_output .* -1);

for i = 1:10000

	tryThese2 = round.(randn(Float64, (10000000, 2)), digits = 5);
	onesCheck2 = round.(sqrt.(sum(tryThese2 .^2, dims = 2)), digits = 6);
	tryThese2_df = DataFrame(tryThese2, :auto);
	onesCheck2_df = DataFrame(onesCheck2, :auto);
	tryThese2_dfOnes = @bind_cols(tryThese2_df, onesCheck2_df);
	tryThese2_df_justOnes = @chain tryThese2_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes2 = tryThese2_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes2)), replace = true), size(goodOnes2))
	goodOnes2_output = @bind_rows(goodOnes2_output, goodOnes2, goodOnes2 .* plusMinusMultiplier);

end

CSV.write("goodOnes2_output.csv", goodOnes2_output, writeheader = false);







### case for 3D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions


goodOnes3_output = DataFrame(I(3), :auto);
goodOnes3_output = convert.(Float64, goodOnes3_output);
goodOnes3_output = @bind_rows(goodOnes3_output, goodOnes3_output .* -1);

for i = 1:10000

	tryThese3 = round.(randn(Float64, (10000000, 3)), digits = 5);
	onesCheck3 = round.(sqrt.(sum(tryThese3 .^2, dims = 2)), digits = 6);
	tryThese3_df = DataFrame(tryThese3, :auto);
	onesCheck3_df = DataFrame(onesCheck3, :auto);
	tryThese3_dfOnes = @bind_cols(tryThese3_df, onesCheck3_df);
	tryThese3_df_justOnes = @chain tryThese3_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes3 = tryThese3_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes3)), replace = true), size(goodOnes3))
	goodOnes3_output = @bind_rows(goodOnes3_output, goodOnes3, goodOnes3 .* plusMinusMultiplier);

end

CSV.write("goodOnes3_output.csv", goodOnes3_output, writeheader = false);




### case for 4D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions

goodOnes4_output = DataFrame(I(4), :auto);
goodOnes4_output = convert.(Float64, goodOnes4_output);
goodOnes4_output = @bind_rows(goodOnes4_output, goodOnes4_output .* -1);

for i = 1:10000

	tryThese4 = round.(randn(Float64, (10000000, 4)), digits = 8);
	onesCheck4 = round.(sqrt.(sum(tryThese4 .^2, dims = 2)), digits = 6);
	tryThese4_df = DataFrame(tryThese4, :auto);
	onesCheck4_df = DataFrame(onesCheck4, :auto);
	tryThese4_dfOnes = @bind_cols(tryThese4_df, onesCheck4_df);
	tryThese4_df_justOnes = @chain tryThese4_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes4 = tryThese4_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes4)), replace = true), size(goodOnes4))
	goodOnes4_output = @bind_rows(goodOnes4_output, goodOnes4, goodOnes4 .* plusMinusMultiplier);

end

CSV.write("goodOnes4_output.csv", goodOnes4_output, writeheader = false);



### case for 5D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions


goodOnes5_output = DataFrame(I(5), :auto);
goodOnes5_output = convert.(Float64, goodOnes5_output);
goodOnes5_output = @bind_rows(goodOnes5_output, goodOnes5_output .* -1);

for i = 1:10000

	tryThese5 = round.(randn(Float64, (10000000, 5)), digits = 5);
	onesCheck5 = round.(sqrt.(sum(tryThese5 .^2, dims = 2)), digits = 6);
	tryThese5_df = DataFrame(tryThese5, :auto);
	onesCheck5_df = DataFrame(onesCheck5, :auto);
	tryThese5_dfOnes = @bind_cols(tryThese5_df, onesCheck5_df);
	tryThese5_df_justOnes = @chain tryThese5_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes5 = tryThese5_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes5)), replace = true), size(goodOnes5))
	goodOnes5_output = @bind_rows(goodOnes5_output, goodOnes5, goodOnes5 .* plusMinusMultiplier);

end

CSV.write("goodOnes5_output.csv", goodOnes5_output, writeheader = false);




### case for 6D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions


goodOnes6_output = DataFrame(I(6), :auto);
goodOnes6_output = convert.(Float64, goodOnes6_output);
goodOnes6_output = @bind_rows(goodOnes6_output, goodOnes6_output .* -1);

for i = 1:10000

	tryThese6 = round.(randn(Float64, (10000000, 6)), digits = 5);
	onesCheck6 = round.(sqrt.(sum(tryThese6 .^2, dims = 2)), digits = 6);
	tryThese6_df = DataFrame(tryThese6, :auto);
	onesCheck6_df = DataFrame(onesCheck6, :auto);
	tryThese6_dfOnes = @bind_cols(tryThese6_df, onesCheck6_df);
	tryThese6_df_justOnes = @chain tryThese6_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes6 = tryThese6_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes6)), replace = true), size(goodOnes6))
	goodOnes6_output = @bind_rows(goodOnes6_output, goodOnes6, goodOnes6 .* plusMinusMultiplier);

end

CSV.write("goodOnes6_output.csv", goodOnes6_output, writeheader = false);




### case for 7D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions


goodOnes7_output = DataFrame(I(7), :auto);
goodOnes7_output = convert.(Float64, goodOnes7_output);
goodOnes7_output = @bind_rows(goodOnes7_output, goodOnes7_output .* -1);

for i = 1:10000

	tryThese7 = round.(randn(Float64, (10000000, 7)), digits = 5);
	onesCheck7 = round.(sqrt.(sum(tryThese7 .^2, dims = 2)), digits = 6);
	tryThese7_df = DataFrame(tryThese7, :auto);
	onesCheck7_df = DataFrame(onesCheck7, :auto);
	tryThese7_dfOnes = @bind_cols(tryThese7_df, onesCheck7_df);
	tryThese7_df_justOnes = @chain tryThese7_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes7 = tryThese7_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes7)), replace = true), size(goodOnes7))
	goodOnes7_output = @bind_rows(goodOnes7_output, goodOnes7, goodOnes7 .* plusMinusMultiplier);

end

CSV.write("goodOnes7_output.csv", goodOnes7_output, writeheader = false);




### case for 8D
using Random, DataFrames, LinearAlgebra, TidierData, CSV, StatsBase, Distributions


goodOnes8_output = DataFrame(I(8), :auto);
goodOnes8_output = convert.(Float64, goodOnes8_output);
goodOnes8_output = @bind_rows(goodOnes8_output, goodOnes8_output .* -1);

for i = 1:10000

	tryThese8 = round.(randn(Float64, (10000000, 8)), digits = 5);
	onesCheck8 = round.(sqrt.(sum(tryThese8 .^2, dims = 2)), digits = 6);
	tryThese8_df = DataFrame(tryThese8, :auto);
	onesCheck8_df = DataFrame(onesCheck8, :auto);
	tryThese8_dfOnes = @bind_cols(tryThese8_df, onesCheck8_df);
	tryThese8_df_justOnes = @chain tryThese8_dfOnes begin
		@filter(x1_1 == 1)
	end;
	goodOnes8 = tryThese8_df_justOnes[!, Not("x1_1")]; 
	plusMinusMultiplier = reshape(sample([1 -1], prod(size(goodOnes8)), replace = true), size(goodOnes8))
	goodOnes8_output = @bind_rows(goodOnes8_output, goodOnes8, goodOnes8 .* plusMinusMultiplier);

end

CSV.write("goodOnes8_output.csv", goodOnes8_output, writeheader = false);