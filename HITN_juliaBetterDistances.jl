### use this code to generate pairwise distances of all rows of csv phylogenetic data

using DataFrames 
using Statistics
using CSV
using Tables

allEvo = CSV.read("allEvoOutput2.csv", DataFrame; header = false);
traitSpace = CSV.read("traitSpaceOutput2.csv", DataFrame; header = false);
# transform all transposed matrices, so calculations go faster downstream

allM = Matrix(allEvo)';
traitM = Matrix(traitSpace)'; 

### code from https://discourse.julialang.org/u/rdeits on juliahub 
### see https://discourse.julialang.org/t/improving-performance-of-a-nested-for-loop/29705/7 
# run on "c_cols" ; translated matrix works much better


function triangle_number(n)
    if iseven(n)
        (n ÷ 2) * (n + 1)
    else
        ((n + 1) ÷ 2) * n
    end
end

function distance_matrix4(c)
    if sum(c) == 0
        cdist = c
    else    
        cdist = Matrix{eltype(c)}(undef, size(c, 1), triangle_number(size(c, 2) - 1))
        col = 1
        for j in 1:(size(c, 2) - 1)
            for h in (j + 1):size(c, 2)
                for k in 1:size(c, 1)
                    cdist[k, col] = abs(c[k, j] - c[k, h])
                end
                col += 1
            end
        end
        return cdist
    end
end



### note that below the matrices are transposed before they're read into the _dist.csv files

allM_dist = distance_matrix4(allM)'; 
traitM_dist = distance_matrix4(traitM)'; 

# write out phylogenetic placekeep files for further text processing

CSV.write("all_dists.csv", DataFrame(Tables.table(allM_dist)));

### make sure you have a .sh script in your working directory 
### might need to change "zsh" to an appropriate shell interpreter (e.g., "sh")
run(`zsh HITN_finalTextProcessing.zsh`)

### make sure to take out the "remove line that contains 'missing'" things into the bash script

phylDist = Matrix(CSV.read("all_dists5.csv", DataFrame; header = false));

### read in seeds value so I can associate these data with the R output
#theseSeeds = CSV.File("seeds.csv", header = true);


###write output object 
#output = [theseSeeds[1 , 1] theseSeeds[1 , 2] median(allDist) minimum(allDist) maximum(allDist) count(x -> x < 4, allDist)];

#CSV.write("HITNjuliaOutput.csv", DataFrame(output), append=true)

d1 = sqrt.(sum(traitM_dist[:, 29].^2, dims=2));
d2 = sqrt.(sum(traitM_dist[:, 29:30].^2, dims=2));
d4 = sqrt.(sum(traitM_dist[:, 29:32].^2, dims=2));
d8 = sqrt.(sum(traitM_dist[:, 29:36].^2, dims=2));
d16 = sqrt.(sum(traitM_dist[:, 29:44].^2, dims=2));
d32 = sqrt.(sum(traitM_dist[:, 29:60].^2, dims=2));
d64 = sqrt.(sum(traitM_dist[:, 29:92].^2, dims=2));
d128 = sqrt.(sum(traitM_dist[:, 29:156].^2, dims=2));
d256 = sqrt.(sum(traitM_dist[:, 29:284].^2, dims=2));
d512 = sqrt.(sum(traitM_dist[:, 29:540].^2, dims=2));
d1024 = sqrt.(sum(traitM_dist[:, 29:1052].^2, dims=2));

### add chebyshev distances below
d1ch = maximum(traitM_dist[:, 29], dims=2);
d2ch = maximum(traitM_dist[:, 29:30], dims=2);
d4ch = maximum(traitM_dist[:, 29:32], dims=2);
d8ch = maximum(traitM_dist[:, 29:36], dims=2);
d16ch = maximum(traitM_dist[:, 29:44], dims=2);
d32ch = maximum(traitM_dist[:, 29:60], dims=2);
d64ch = maximum(traitM_dist[:, 29:92], dims=2);
d128ch = maximum(traitM_dist[:, 29:156], dims=2);
d256ch = maximum(traitM_dist[:, 29:284], dims=2);
d512ch = maximum(traitM_dist[:, 29:540], dims=2);
d1024ch = maximum(traitM_dist[:, 29:1052], dims=2);


###write distance output dataframe 
distOutput = DataFrame([phylDist d1 d2 d4 d8 d16 d32 d64 d128 d256 d512 d1024 d1ch d2ch d4ch d8ch d16ch d32ch d64ch d128ch d256ch d512ch d1024ch], ["phyl", "D1", "D2", "D4", "D8", "D16", "D32", "D64", "D128", "D256", "D512", "D1024", "D1ch", "D2ch", "D4ch", "D8ch", "D16ch", "D32ch", "D64ch", "D128ch", "D256ch", "D512ch", "D1024ch"]);

CSV.write("HITNjuliaDistOutput.csv", distOutput)

### this is def not the best way to do this... use "DataFrames.stack" 
longDistOutput = stack(distOutput, [:D1, :D2, :D4, :D8, :D16, :D32, :D64, :D128, :D256, :D512, :D1024, :D1ch, :D2ch, :D4ch, :D8ch, :D16ch, :D32ch, :D64ch, :D128ch, :D256ch, :D512ch, :D1024ch]);

CSV.write("HITNjuliaLongDistOutput.csv", longDistOutput)


### this probably isn't the best way to get distOriginal... but it seemed fun? idk... 
distOriginal = CSV.read("HITNjuliaDistOutput.csv", DataFrame; header = true);
distOriginalShuffled = insertcols!(distOriginal, :randomSorter => randn(size(distOriginal, 1)));
distOriginalShuffled = sort!(distOriginalShuffled, :randomSorter);


distsSubset = distOriginal[1:5, :]; 


 for i in 1:maximum(distOriginal.phyl)
 	if size(filter(row -> row.phyl == i, distOriginal), 1) < 10
 		distsSubset = distsSubset
 	else
       distsSubset = DataFrame(vcat(distsSubset, filter(row -> row.phyl == i, distOriginal)[1:10, :])); 
    end
 end 

distsSubset = distsSubset[6:size(distsSubset, 1), :];

CSV.write("distsSubset.csv", distsSubset, bufsize = 4194304000);

tallDistsSubset = stack(distsSubset[:, 1:12], 2:12);

CSV.write("tallDistsSubset.csv", tallDistsSubset, bufsize = 4194304000);
